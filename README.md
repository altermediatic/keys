# Clefs

## SSH

- Ajout de la(les) votre(s): `ssh-add -L >> ssh`, puis éditez ce fichier pour mettre votre pseudo en commentaire
- Déploiement:

```bash
cd
git clone git@framagit.org:oxyta.net/keys.git
cd .ssh
rm authorized_keys
ln -s ../keys/ssh authorized_keys
```

## GPG

### Clefs publiques

- Ajout de la votre: `gpg --armor --export $KEY > public_keys/$pseudo.asc`
- Import: `gpg --import public_keys/*.asc`

### Fingerprint

`gpg --fingerprint $KEY |head -n2|tail -n1|sed 's/ //g' >> gpg`
